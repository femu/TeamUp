var ip = "http://127.0.0.1:5000/v1/"

function login() {
    email = document.getElementById("login-email").value;
    password = document.getElementById("login-password").value;
    credentials = {"email":email,"password":password} 
    credentials = JSON.stringify(credentials)
    
    $.ajax({
        url: ip+"user/signin",
        type: 'POST',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        data: credentials,
        success: function(response, textStatus, request){
            var token  = request.getResponseHeader("Authorization")
            var name = response.username
            var user_id = response.user_id
            //Token and Name saved local
            localStorage.setItem('token', token)
            localStorage.setItem('name', name)
            localStorage.setItem('user_id', user_id)

            location.assign("index.html")
        },

        error: function(response) {
            document.getElementById("background").innerHTML = '<div style="margin-bottom: 0;position: absolute; width: 100%;" class="alert alert-danger alert-dismissible fade show"><strong>Wrong Credentials! </strong> Please try again or contact the Admin<button type="button" class="close" data-dismiss="alert">&times;</button></div>' + document.getElementById("background").innerHTML;
        }
    });
};

function generateTeam() {
    let team_name = document.getElementById("newTeam-teamname").value;
    let token = localStorage.getItem('token');
    let user_id = localStorage.getItem('user_id');
    if (user_id == null){
        return null
    }
    post_data = JSON.stringify({"team_name":team_name,"user_id": user_id});
    $.ajax({
        async:false,
        url: ip+"team/create",
        type: 'POST',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        data: post_data,
        success: function(response, textStatus, request){
                $("#newTeam-1").attr("style", "display:none");
                $("#newTeam-4").attr("style", "display:flex");
                document.getElementById("newTeam-code").innerHTML = response.share_code;
        },

        error: function(response) {
              document.getElementById("background").innerHTML = '<div style="margin-bottom: 0;position: absolute; width: 100%;" class="alert alert-danger alert-dismissible fade show"><strong>Error!</strong> Please try again or contact the Admin<button type="button" class="close" data-dismiss="alert">&times;</button></div>' + document.getElementById("background").innerHTML;
        }
     });
    
}

function generateEmail() {
    var formattedBody = "Cheers, \n\nCome and join my team. \Just go to TeamUp.com (demo link) and click on Join Team. \nThere you just have to add the code from the subject \n\nGreetings and see you soon!";
    var subject = "Access code for TeamUp: : " + document.getElementById("newTeam-code").innerHTML
    var mailToLink = "mailto:?subject="+subject+"&body=" + encodeURIComponent(formattedBody);
    document.getElementById("emailgenerator").href = mailToLink;
};


function joinTeam(user_id, share_code){
    if (share_code == null){
        share_code = document.getElementById("joinTeam-code").value
    }
    if (user_id == null){
        user_id = localStorage.getItem("user_id")
    }
    if (user_id == null){
        $("#newTeam-1").attr("style", "display:none");
        $("#newTeam-2").attr("style", "display:flex");
        $("#newTeam-3").attr("style", "display:none");
        $("#newTeam-4").attr("style", "display:none");
    }
    else{

        post_data = JSON.stringify({"share_code":share_code, "user_id":user_id})
        $.ajax({
            async:false,
            url: ip+"team/join",
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: post_data,
            success: function(response, textStatus, request){
                $("#newTeam-1").attr("style", "display:none");
                $("#newTeam-2").attr("style", "display:none");
                $("#newTeam-3").attr("style", "display:flex");
            },

            error: function(response) {
                document.getElementById("background").innerHTML = '<div style="margin-bottom: 0;position: absolute; width: 100%;" class="alert alert-danger alert-dismissible fade show"><strong>Error!</strong> Please try again or contact the Admin<button type="button" class="close" data-dismiss="alert">&times;</button></div>' + document.getElementById("background").innerHTML;
            }
        });
    };
};

function signup(){
    share_code = document.getElementById("joinTeam-code").value
    firstname = document.getElementById("joinTeam-firstname").value
    lastname = document.getElementById("joinTeam-lastname").value
    username = document.getElementById("joinTeam-username").value
    password = document.getElementById("joinTeam-password").value
    email = document.getElementById("joinTeam-email").value
    
    post_data = JSON.stringify({
        "username": username,
        "email": email,
        "lastName": lastname,
        "firstName": firstname,
        "password": password
      })
    $.ajax({
        async:false,
        url: ip+"user/signup",
        type: 'POST',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        data: post_data,
        success: function(response, textStatus, request){
            joinTeam(response.user_id, share_code)

        },

        error: function(response) {
            document.getElementById("background").innerHTML = '<div style="margin-bottom: 0;position: absolute; width: 100%;" class="alert alert-danger alert-dismissible fade show"><strong>Error!</strong> Please try again or contact the Admin<button type="button" class="close" data-dismiss="alert">&times;</button></div>' + document.getElementById("background").innerHTML;
        }
    });

};