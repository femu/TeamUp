from flask_restx import Namespace, fields

class TeamDto():
    api = Namespace('teams', description='team related operations')
    model = api.model('team', {
        'id': fields.Integer(readonly=True, attribute='team_id'),
        'creator_id': fields.String(required=True),
        'team_name': fields.String(required=True),
        'share_code': fields.String(required=True),
        'creation_date': fields.String(required=True),
        'members': fields.List(fields.Integer(attribute='user_id'), attribute="members"),
        'surveys': fields.List(
            fields.Integer(attribute='survey_id'),
            attribute='surveys'
        )
    })
    model_create = api.model('create', {
        'user_id': fields.Integer(required=True),
        'team_name': fields.String(required=True)
    })
    model_join = api.model('join', {
        'share_code': fields.String(required=True),
        'user_id': fields.Integer(required=True)
    })