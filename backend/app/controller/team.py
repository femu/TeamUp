from datetime import datetime
import random
from flask_restx import Resource, Namespace
from flask import request
from flask_cors import cross_origin
from app.service.team_service import *
from app.service.auth_service import verify_abort
from app.util.team_dto import TeamDTO
from flask_cors import CORS, cross_origin

api = TeamDTO.api
_team = TeamDTO.model
_username = TeamDTO.model_username
_login = TeamDTO.model_login
_token = TeamDTO.model_token
_signup = TeamDTO.model_signup

auth_parser = api.parser()
auth_parser.add_argument('Authorization', location='headers')


@api.route('/<int:team_id>')
class TeamById(Resource):
    @api.doc('Team by ID')
    @api.marshal_with(_team)
    @api.response(404, 'Team not found')
    def get(self, user_id):
        return get_team_by_id(team_id)


@api.route('/signup')
class SignUp(Resource):
    @api.doc('Signup')
    @api.expect(_signup, envelope='data')
    @api.response(404, 'Errors with SignUp')
    def post(self):
        data = request.json
        try:
            accounts = data["accounts"]
        except:
            accounts = None
        register(username=data["username"], email=data["email"], lastName=data["lastName"], firstName=data["firstName"],
                 birthdate=data["birthdate"], socialMediaProfiles=accounts, password=data["password"])


@api.route('/usernames')
class Usernames(Resource):
    @api.doc('Usernames')
    # @api.marshal_list_with(envelope='data')
    @api.response(404, 'Errors with Usernames')
    def get(self):
        usernames = get_all_usernames()

        return {"data": usernames}


@api.route('/signin')
class Login(Resource):
    @api.doc('Signin')
    @api.expect(_login, validate=True)
    @api.response(404, 'Wrong credentials or user not found')
    # @api.marshal_with(_token, envelope='data')
    @cross_origin(supports_credentials=True)
    def post(self):
        email = request.json["email"]
        password = request.json["password"]
        token, username = login(email, password)
        return {"message": "Successfully authenticated", "username": username}, 201, {"Authorization": token,
                                                                                      "Access-Control-Expose-Headers": "Authorization"}


@api.route('/<string:username>')
class UserByUsername(Resource):
    @api.doc('Get User by username')
    @api.marshal_with(_user)
    @api.expect(auth_parser, validate=True)
    @api.response(404, 'Wrong username or user not found')
    def get(self, username):
        token = auth_parser.parse_args()['Authorization']
        verify_abort(token)
        user = get_user_by_username(username)
        requester = get_user_from_token(token)
        user.follows = requester.is_following(user)

        if (user.portfolio):
            user.portfolio = get_portfolio_by_user_id_currency(user.user_id)
        user.correct_trades = 0
        user.wrong_trades = 0
        len_trades = len(user.trades)
        if (len_trades > 0):
            user.correct_trades = random.randint(0, len_trades)
            user.wrong_trades = len_trades - user.correct_trades
        return user

@api.route('/verify-token')
class VerifyToken(Resource):
    @api.expect(auth_parser, validate=True)
    @api.doc('Verifies Token')
    def post(self):
        token = auth_parser.parse_args()['Authorization']
        if verify_abort(token):
            response_object = {
                'status': 'success',
                'message': 'Token is valid'
            }
            return response_object, 201

